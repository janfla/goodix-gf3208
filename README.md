# Goodix GF3208
This repository contains original source code for Goodix GF3208 kernel module ported from Cyanogen Mod to modern Linux system.

Original module code is available [here](https://github.com/yangyangnau/android_kernel_xiaomi_msm8937/tree/cm-13.0/drivers/fingerprint/goodix).

# Basic info
The device is using SPI bus for communication. It is connected via USB. More information is available [here](https://lists.freedesktop.org/archives/fprint/2018-February/000999.html).

# Compilation
The compilation was tested in Archlinux. Be sure the kernel headers package is installed.

```
cd goodix
patch -p1 < ../jf.patch
make
```

Inserting the module by `sudo insmod gf3208.ko` works:
```
$ dmesg | tail
[ 4983.703675] gf:gf_init, entry
[ 4983.703676] --------gf_init start.--------
[ 4983.703713]  status = 0x0
[ 4983.703713] gf:gf_init, exit
[ 4983.703714] --------gf_init end---OK.--------
```

```
$ lsmod | grep gf3208
gf3208                     28672  0
```

# goodix-gf3208-dkms
This directory contains the script for creating AUR package with Goodix GF3208 kernel module.

# Summary
Still under development.

# URLs
- https://bbs.archlinux.org/viewtopic.php?id=234844

# Notes
- https://gitlab.freedesktop.org/libfprint/libfprint/-/issues/161
